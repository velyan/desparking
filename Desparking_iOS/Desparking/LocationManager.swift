//
//  LocationManager.swift
//  Desparking
//
//  Created by Velislava Yanchina on 05/08/2014.
//  Copyright (c) 2014 despark. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class LocationManager : NSObject, CLLocationManagerDelegate {
    
    var locationManager: CLLocationManager! = nil
    var dataManager : DataManager = DataManager()
    var notificationsManager : LocalNotificationsManager = LocalNotificationsManager()
    
    override init() {
        super.init()
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.initializeRegionMonitoring()
    }
    
    func dictToRegion (dict : NSDictionary) -> CLRegion {
        
        let indentifier = dict.valueForKey("identifier") as NSString
        let latitude = dict.valueForKey("latitude") as CLLocationDegrees
        let longitude = dict.valueForKey("longitude") as CLLocationDegrees
        var regionRadius = dict.valueForKey("radius") as CLLocationDistance
        
        var centerCoordinate = CLLocationCoordinate2DMake(latitude, longitude)
        
        if (regionRadius > locationManager.maximumRegionMonitoringDistance) {
            regionRadius = locationManager.maximumRegionMonitoringDistance
        }
        
        var region = CLCircularRegion(center: centerCoordinate, radius: regionRadius, identifier : indentifier)
        
        return region
    }
    
    func initializeRegionMonitoring() {
        if (!CLLocationManager.locationServicesEnabled()){
            println("Location services are not enabled")
        }
        if (!CLLocationManager.isMonitoringAvailableForClass(CLRegion)) {
            println("Monitoring is not available for class")
        }
        let status = CLLocationManager.authorizationStatus()
        if (status == CLAuthorizationStatus.NotDetermined ||
            status == CLAuthorizationStatus.Restricted) {
                if (locationManager.respondsToSelector(Selector("requestAlwaysAuthorization"))) {
                    locationManager.requestAlwaysAuthorization()
                }
                println("Location services are not enabled")
                
        }
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringForRegion(self.dictToRegion(["identifier" : "parking1", "latitude" : 42.698528, "longitude" : 23.328777, "radius" : 50 ]))
    }
    
    // MARK: Location manager delegate
    func locationManager(manager: CLLocationManager!, didEnterRegion region: CLRegion!) {
        println("Did enter region!")
        dataManager.getFreeSpotsCount() { (freeSpotsCount : NSNumber) in
            self.notificationsManager.scheduleParkingSpotsNotification(freeSpotsCount)
        }
        NSNotificationCenter.defaultCenter().postNotificationName("DidEnterRegion", object: nil)
    }
    
    func locationManager(manager: CLLocationManager!, didExitRegion region: CLRegion!) {
        println("Did exit region!")
        NSNotificationCenter.defaultCenter().postNotificationName("DidExitRegion", object: nil)
    }
//
//    func locationManager(manager: CLLocationManager!, didStartMonitoringForRegion region: CLRegion!) {
//        println("Did start monitoring for region")
//    }
//    
//    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
//        println("Did update locations")
//        println("locations = \(locations)")
//    }
//    
//    func locationManager(manager: CLLocationManager!, didDetermineState state: CLRegionState, forRegion region: CLRegion!) {
//        println("Did determine state")
//    }
//    
//    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus)  {
//        println("Did change authorization status")
//        
//    }
    
}