//
//  ViewController.swift
//  Desparking
//
//  Created by Velislava Yanchina on 01/08/2014.
//  Copyright (c) 2014 despark. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var dataManager : DataManager = DataManager()
    var notificationsManager : LocalNotificationsManager = LocalNotificationsManager()
    
    @IBOutlet weak var spacesCountButton: UIButton!
    @IBOutlet weak var parkedButton: UIButton!
    @IBOutlet weak var timeProgressFillImageView: UIImageView!
    @IBOutlet weak var timeProgressBackgroundImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataManager.getFreeSpotsCount(){(freeSpotsCount : NSNumber) in
            println("There are  \(freeSpotsCount) spots available!")
            self.spacesCountButton.setAttributedTitle(self.attributedTitleWithValue(freeSpotsCount), forState: .Normal)
        }
                
        self.setupTimeProgress()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("didEnterRegion"), name: "DidEnterRegion", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("didExitRegion"), name: "DidExitRegion", object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewDidLoad()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//***** Notification Center *****
    func didEnterRegion(){
        
    }

    func didExitRegion(){

    }
    
//***** Actions *****
    @IBAction func didPark(sender: AnyObject) {
        self.notificationsManager.scheduleReminderNotification()
    }
    
    func attributedTitleWithValue(value: NSNumber) -> NSAttributedString {
        let title : NSString = "\(value)\n places"
        var attributedTitle = NSMutableAttributedString(string: title)
        
        let fontTitle = UIFont(name: "HelveticaNeue", size: 64.0)
        let rangeOfTitle = NSMakeRange(0, title.rangeOfString("\n").location)
        let fontSubtitle = UIFont(name: "HelveticaNeue", size: 30.0)
        let rangeOfSubtitle = title.rangeOfString(" places")
        
        attributedTitle.addAttribute(NSFontAttributeName, value: fontTitle, range: rangeOfTitle)
        attributedTitle.addAttribute(NSFontAttributeName, value: fontSubtitle, range: rangeOfSubtitle)
        attributedTitle.addAttribute(NSForegroundColorAttributeName, value: UIColor.whiteColor(), range: NSMakeRange(0, title.length))
        
        return attributedTitle
    }
    
    func setupTimeProgress() {
        let progressImageCapInsets = UIEdgeInsetsMake(0, 20, 0, 20)
        let progressFillImage = UIImage(named:"timer_red").resizableImageWithCapInsets(progressImageCapInsets, resizingMode: .Stretch)
        var mask = CALayer()
        mask.contents = progressFillImage.CGImage
        
        let width = CGFloat(Float(self.timeProgressBackgroundImageView.layer.bounds.width) * Float(self.calculateProgress()))
        println("Width is \(width)")
        mask.bounds = CGRectMake(0, 0, width, self.timeProgressBackgroundImageView.layer.bounds.height)
        
        self.timeProgressFillImageView.layer.mask = mask
        self.timeProgressFillImageView.layer.masksToBounds = true
    }
    
    func calculateProgress() -> Double {
        var currentDate = NSDate(timeIntervalSinceNow: 0)
        let calendar = NSCalendar.autoupdatingCurrentCalendar()
        var components =  calendar.components(NSCalendarUnit.CalendarUnitYear | NSCalendarUnit.CalendarUnitMonth | NSCalendarUnit.CalendarUnitDay, fromDate: currentDate)
        components.hour = 8
        components.minute = 0
        components.second = 0
        let startOfTheDay = calendar.dateFromComponents(components)

        var workHours : Double = 11 * 60 * 60 // 11 hours per day
        
        var progress = Double(currentDate.timeIntervalSince1970 - startOfTheDay!.timeIntervalSince1970) / workHours
        return progress
    }
    
    
}

