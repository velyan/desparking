//
//  LocalNotificationsManager.swift
//  Desparking
//
//  Created by Velislava Yanchina on 29/08/2014.
//  Copyright (c) 2014 despark. All rights reserved.
//

import Foundation

class LocalNotificationsManager : NSObject {

    func scheduleParkingSpotsNotification(parkingSpots: NSNumber) {
        var localNotification:UILocalNotification = UILocalNotification()
        localNotification.alertBody = "There are \(parkingSpots) spots available"
        localNotification.fireDate = NSDate(timeIntervalSinceNow: 0)
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
    }
    
    func scheduleReminderNotification () {
        
        var now = NSDate(timeIntervalSinceNow: 0)
        let calendar = NSCalendar(identifier: NSGregorianCalendar)
        let components = calendar.components(.CalendarUnitYear | .CalendarUnitMonth | .CalendarUnitDay, fromDate: now) as NSDateComponents
        components.setValue(18, forComponent: .CalendarUnitHour)
        components.setValue(45, forComponent: .CalendarUnitMinute)
        let todayAt7pm = calendar.dateFromComponents(components)
        
        
        var localNotification:UILocalNotification = UILocalNotification()
        localNotification.alertBody = "Don't forget your car here please."
        localNotification.fireDate = todayAt7pm
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
    }
}